from django.db import models

# Create your models here.
class Donation(models.Model) :
    program = models.CharField(max_length = 200)
    nama_donatur = models.CharField(max_length = 200)
    email_donatur = models.EmailField(max_length = 200)
    uang_donasi = models.IntegerField(max_length= 12)
    waktu_donasi = models.DateTimeField()
    id_program = models.IntegerField(default=0)

    def __str__(self) :
        output = str(self.program) + " " + str(self.email_donatur) + " " + str(self.uang_donasi)
        return output
