from django.urls import re_path
from .views import registerView, saveRegisterView
#url for app

urlpatterns = [
    re_path(r'^register/$', registerView, name='form'),
    re_path(r'^register/savelul/$', saveRegisterView, name='save')
]