# Suptix - Group 7 PPW-A 2018

## Overview
Proyek CrowdFunding untuk memenuhi tugas PPW

## Members
* Aziz Fikri Hudaya - 1706979184
* Ervan Adiwijaya Haryadi - 1706074871
* Ferro Geraldi Hardian - 1706028612
* Reynaldo Wijaya Hendry - 1706028625

## Job Description
### Group Project 1
* Aziz F.H.     : Register page
* Ervan A.H.    : Donation page
* Ferro G.H.    : Home page ; News page ; Navigation Bar
* Reynaldo W.H. : Program(s) page

### Group Project 2
* Aziz F.H.     : About page ; 
* Ervan A.H.    : Donation update & synchronization with login ; program & news content update
* Ferro G.H.    : Login OAuth with Google
* Reynaldo W.H. : Donation list of currently logged in user

## Application Status
[![pipeline status](https://gitlab.com/Kel7-PPWA-2018/suptix/badges/master/pipeline.svg)](https://gitlab.com/Kel7-PPWA-2018/suptix/commits/master)
[![coverage report](https://gitlab.com/Kel7-PPWA-2018/suptix/badges/master/coverage.svg)](https://gitlab.com/Kel7-PPWA-2018/suptix/commits/master)

## Link Heroku Application
[https://ppwa-suptix.herokuapp.com/](https://ppwa-suptix.herokuapp.com/)