from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.
class Register(models.Model):
    nama_donatur = models.CharField(max_length = 50)
    donatur_ttl = models.DateField()
    donatur_email = models.EmailField(max_length = 100, unique=True,)
    donatur_password = models.CharField(max_length = 50)

    def __str__(self):
        return self.donatur_email