from django.test import TestCase, Client
from .views import program
from .models import Program
from app_donate.models import Donation
from datetime import datetime
from django.contrib.auth.models import User


# Create your tests here.

class ProgramPageTest(TestCase) :

    def test_url_is_exist(self):
        response = Client().get('/program/')
        self.assertEqual(response.status_code, 200)

    def test_program_is_exist(self):
        title = "Orang paling ganteng tahun 2018"
        headline = "Setiap tahun akan diadakan acara pemilihan orang terganteng di dunia. Selama 19 tahun belakangan ini acara tersebut selalu dimenangkan oleh Reynaldo. Tahun ini ternyata Reynaldo kembali memenangkan perlombaan orang paling ganteng sedunia"
        Program.objects.create(title = title, headline = headline)

        html_response = self.client.get('/program/').content.decode('utf8')
        self.assertIn(title, html_response)
        self.assertIn(headline, html_response)

    def test_get_program_invalid(self) :
        title = "Orang paling ganteng tahun 2018"
        headline = "Setiap tahun akan diadakan acara pemilihan orang terganteng di dunia. Selama 19 tahun belakangan ini acara tersebut selalu dimenangkan oleh Reynaldo. Tahun ini ternyata Reynaldo kembali memenangkan perlombaan orang paling ganteng sedunia"
        Program.objects.create(title = title, headline = headline)

        html_response = self.client.get('/program/?id=asdasda').content.decode('utf8')
        self.assertIn(title, html_response)
        self.assertIn(headline, html_response)

    def test_get_program_valid(self) :
        title = "Orang paling ganteng tahun 2018"
        description = "Setiap tahun akan diadakan acara pemilihan orang terganteng di dunia. Selama 19 tahun belakangan ini acara tersebut selalu dimenangkan oleh Reynaldo. Tahun ini ternyata Reynaldo kembali memenangkan perlombaan orang paling ganteng sedunia"
        Program.objects.create(title = title, description = description)

        html_response = self.client.get('/program/?id=1').content.decode('utf8')
        self.assertIn(title, html_response)
        self.assertIn(description, html_response)

    def test_list_donasi(self):
        self.client = Client()

        self.user = User.objects.create(username='testuser', password='12345', is_active=True, is_staff=True, is_superuser=True)
        self.user.set_password('hello')
        self.user.save()

        login = self.client.login(username='testuser', password='hello', email = "reynaldo.ganteng@gmail.com")
        response = self.client.get('/list_donasi/')

        self.assertEqual(response.status_code, 200)

    '''def test_check_donasi(self):
        self.client = Client()

        self.user = User.objects.create(username='testuser', password='12345', is_active=True, is_staff=True, is_superuser=True)
        self.user.set_password('hello')
        self.user.save()

        login = self.client.login(username='testuser', password='hello', email = "reynaldo.ganteng@gmail.com")

        email = "reynaldo.ganteng@gmail.com"
        uang = 100000
        Donation.objects.create(email_donatur = email, uang_donasi = uang, id_program = '1', program="Rey-ganteng", waktu_donasi = datetime.now())

        html_response = self.client.get('/list_donasi/').content.decode('utf8')

        self.assertIn(str(uang), html_response)'''
