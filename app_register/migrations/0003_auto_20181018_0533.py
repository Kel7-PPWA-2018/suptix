# Generated by Django 2.1.1 on 2018-10-17 22:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_register', '0002_auto_20181017_1608'),
    ]

    operations = [
        migrations.AlterField(
            model_name='register',
            name='donatur_email',
            field=models.EmailField(max_length=100, unique=True),
        ),
        migrations.AlterField(
            model_name='register',
            name='nama_donatur',
            field=models.CharField(max_length=50),
        ),
    ]
