from django import forms
from .models import Register

class Add_Register_Form(forms.ModelForm):
    class Meta:
        model = Register
        fields = ['nama','ttl','email','password',]


    nama_donatur_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'text your name here',
    }

    donatur_ttl_attrs = {
        'type':'date',
        'class' : 'form-control' 
    }

    donatur_email_attrs = {
        'class' : 'form-control',
        'placeholder': 'fulan123@gmail.com'
    }

    donatur_password_attrs = {
        'type' : 'password', 
        'class' : 'form-control',
        'placeholder':'text your password here',
    }
    
    nama = forms.CharField(required=True, label = 'Full Name', widget=forms.TextInput(attrs = nama_donatur_attrs))
    email = forms.EmailField(required=True, label = 'Email', widget=forms.TextInput(attrs = donatur_email_attrs), error_messages={'unique': 'email already taken!'})
    ttl = forms.DateField(required=True, label = 'Birth Date', widget=forms.DateTimeInput(attrs= donatur_ttl_attrs), input_formats=['%Y-%m-%d'])
    password = forms.CharField(required=True, label = 'Password', widget=forms.TextInput(attrs = donatur_password_attrs))


    

    