from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.utils import timezone
from .models import Program
from app_donate.models import Donation

# Create your views here.

def program(request) :
    list_program = Program.objects.all()
    try:
        url = int(request.GET.get('id', 0))
    except :
        url = 0
    if(url == 0):
        context = {"list_program" : list_program}
        return render(request, "program.html", context)
    else:
        program = Program.objects.all().filter(id = url).values()[0]
        sisa_waktu = timezone.now() - program["time"]
        sisa_waktu = program["target_hari"] - sisa_waktu.days
        list_donatur = Donation.objects.all().filter(id_program = url)
        uang_terkumpul = 0
        for donatur in list_donatur:
            uang_terkumpul += donatur.uang_donasi

        print(program)
        context = {"program" : program, "sisa_waktu" : sisa_waktu, "list_donatur": list_donatur, "uang_terkumpul": uang_terkumpul}
        return render(request, "program_satuan.html", context)

def list_donasi(request):
    if not request.user.is_authenticated:
        return redirect("/")
    else:
        return render(request, "list_donasi.html")

def get_list_donasi(request):
    list_donasi = Donation.objects.all().filter(email_donatur = request.user.email)

    uang_donasi = 0
    for donasi in list_donasi:
        uang_donasi += donasi.uang_donasi

    data = list(list_donasi.values())
    return JsonResponse({"data" : data, "uang_donasi" : uang_donasi})
