from django.test import TestCase, Client
from django.urls import resolve
from .models import Testimoni
from .views import viewAbout, saveComment
from .forms import Add_Testimoni_Form

class AppRegisterTest(TestCase):

	def test_about_app_exist(self):
		response = Client().get('/about/')
		self.assertEqual(response.status_code, 200)
	def test_about_templates(self):
		response = self.client.get('/about/')
		self.assertTemplateUsed(response, 'about.html')

	def test_registerview_func(self):
		found = resolve('/about/')
		self.assertEqual(found.func, viewAbout)

	def test_registerview_succes_func(self):
		found = resolve('/about/savelul/')
		self.assertEqual(found.func, saveComment)

	def test_forms_is_valid(self):
		this_form = Add_Testimoni_Form(data = {
			'name' : 'Aziz', 
			'testimoni':'aaaaaaaa iya iya iyayaya',
			})
		self.assertTrue(this_form.is_valid())

	def test_model_can_create_new_status(self):
		new_form = Testimoni.objects.create(name = 'anonimus', testimoni = 'jomblo')
		count = Testimoni.objects.all().count()
		self.assertEqual(count, 1)
		self.assertEqual(str(new_form), 'anonimus')