from django import forms
from .models import Testimoni

class Add_Testimoni_Form(forms.ModelForm):
	class Meta:
		model = Testimoni
		fields = ['testimoni']

	testimoni_attrs = {
		'type': 'text',
        'row' : 4,
        'cols' : 50,
        'class': 'form-control',
        'placeholder':'Text your review here ',
	}
	testimoni = forms.CharField(required=True, label = "Add Testimoni", widget=forms.Textarea(attrs=testimoni_attrs))