from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

from .models import News
from app_program.models import Program

# Create your views here.
response = {}

def index(request, num=0) :
    if num == 0:
        news = News.objects.all()[::-1]
        for new in news:
            new.date = new.date.strftime("%B %d, %Y")
        response['news'] = news
        return render(request, "index.html", response)
    else:
        try:
            news = News.objects.get(id=num)
            news.date = news.date.strftime("%B %d, %Y")
            programs = Program.objects.filter(id=num)
            response['news'] = news
            response['programs'] = programs
            return render(request, "news.html", response)
        except:
            return HttpResponseRedirect('/')

def log_out(request):
    logout(request)
    return redirect('/')
