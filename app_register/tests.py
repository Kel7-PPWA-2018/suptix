from django.test import TestCase, Client
from django.urls import resolve
from .models import Register
from .views import registerView, saveRegisterView
from .forms import Add_Register_Form

class AppRegisterTest(TestCase):

#url check

    def test_register_app_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_success(self):
        response = Client().get('/register/savelul/')
        self.assertEqual(response.status_code, 302)

#templatecheck

    def test_register_templates(self):
        response = self.client.get('/register/')
        self.assertTemplateUsed(response, 'register.html')

#function views check
    def test_registerview_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, registerView)

    def test_registerview_succes_func(self):
        found = resolve('/register/savelul/')
        self.assertEqual(found.func, saveRegisterView)

    def test_forms_is_valid(self):
        this_form = Add_Register_Form(data = {
            'nama' : 'anonimus',
            'ttl':'2018-12-12',
            'email' : 'azizlul@gmail.com',
            'password' : 'youscrublel',
            })
        self.assertTrue(this_form.is_valid())


    def test_can_save_a_POST_request(self):
        response = self.client.post('/register/savelul/', data={
            'nama' : 'anonimus',
            'ttl':'2018-12-12',
            'email' : 'azizlul@gmail.com',
            'password' : 'youscrublel',
            })
        count = Register.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(response.status_code, 200)
