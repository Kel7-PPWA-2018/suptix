from django.shortcuts import render
from .models import Testimoni
from .forms import Add_Testimoni_Form
from django.http import HttpResponseRedirect, JsonResponse
from django.db import IntegrityError

response = {}
def viewAbout(request):
	display = Testimoni.objects.all()[::-1]
	response["data"] = display
	response["form"] = Add_Testimoni_Form
	html = "about.html"
	return render(request, html, response)

def saveComment(request):
	data = []		
	form = Add_Testimoni_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		print("meeeoong")
		bucket = {}
		bucket['name'] = request.user.first_name
		bucket['testimoni'] = request.POST['testimoni']
		model = Testimoni(name = bucket['name'] , testimoni = bucket['testimoni'])
		model.save()
		data.append(bucket)
		return JsonResponse({'data' : data}) 
	else:
		return HttpResponseRedirect("/about/")
	

	



