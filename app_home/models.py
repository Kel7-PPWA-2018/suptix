from django.db import models

# Create your models here.
class News(models.Model):
    title = models.CharField(max_length=50)
    short_desc = models.CharField(max_length=200)
    long_desc = models.TextField()
    date = models.DateTimeField()
    image = models.URLField(default='https://files.catbox.moe/6aq8g6.png', max_length=300)

    def __str__(self):
        return self.title
