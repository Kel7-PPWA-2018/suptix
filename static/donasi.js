$(function(){
  $.ajax("/get_list_donasi",
    {
      success: function (result) {
        const data = result["data"]
        let tambahan_data = "";
        for (let i = 0; i < data.length; i++) {
            tambahan_data += `<div class='container-fluid'> <h4> Nama Program : `+ data[i]["program"] + `</h4> <h5> Uang yang didonasikan : ` +
            data[i]["uang_donasi"] + `</h5>
            </div><br>`
        }

        $("#listdonasi").html(tambahan_data)
        $("#uangdonasi").html(`Total uang yang telah Anda donasikan: ` + result["uang_donasi"])
      }
    })
});
