from django.apps import AppConfig


class AppProgramConfig(AppConfig):
    name = 'app_program'
