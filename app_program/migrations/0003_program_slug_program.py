# Generated by Django 2.1.1 on 2018-10-18 06:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_program', '0002_auto_20181018_0650'),
    ]

    operations = [
        migrations.AddField(
            model_name='program',
            name='slug_program',
            field=models.CharField(default='rey-ganteng', max_length=50),
        ),
    ]
