//$(#tombol_kirim).("submit", function(){
//	alert(#id_testimoni);
//})

$('#formtesti').on('submit', function(event){
    event.preventDefault();
    console.log("form submitted!")  // sanity check
    create_post();
});

function create_post() {
	console.log("create post is working!") // sanity check
	var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
	function csrfSafeMethod(method) {
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
	$.ajaxSetup({
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});

    $.ajax({
        url : "savelul/", // the endpoint
        type : "POST", // http method
		data : 
		{ testimoni : $('#id_testimoni').val() }, // data sent with the post request

        // handle a successful response
        success : function(response) {
			var lists = response["data"];
			var bucket = "";
			for(var i = 0; i< lists.length; i++){
				bucket += "<div class = 'statusdiv'>";
				bucket += "<p class = 'statustext'>"+"Name : <b>"+lists[i]['name']+"</b></p>";
				bucket += "<p class = 'statustext'><i>"+'" '+lists[i]['testimoni']+' "'+"</i></p>";
				bucket += "</div>";
			}
			$('#datajson').prepend(bucket)
            $('#id_testimoni').val(''); // remove the value from the input
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};
