from django.test import TestCase, Client
from django.urls import resolve

from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from django.http import HttpRequest
from .views import donate_fail_view,donate_post,donate_success_view,donate_view
from .models import Donation
from app_register.models import Register
from datetime import date
from django.contrib.sessions.models import Session

# Create your tests here.
class Suptix_Donate (TestCase) :

#url routing test
    def test_donate_page_exist(self) :
        response = Client().get('/donate/')
        self.assertEqual(response.status_code, 200)

    def test_donate_success_page_exist(self) :
        response = Client().get('/donate/success')
        self.assertEqual(response.status_code, 200)

    def test_donate_fail_page_exist(self) :
        response = Client().get('/donate/fail')
        self.assertEqual(response.status_code, 200)

#template test
    def test_use_donate_template(self) :
        response = Client().get('/donate/')
        self.assertTemplateUsed(response, 'donasi.html')

    def test_use_donate_success_template(self) :
        response = Client().get('/donate/success')
        self.assertTemplateUsed(response, 'donasi_sukses.html')

    def test_donate_fail_page_template(self) :
        response = Client().get('/donate/fail')
        self.assertTemplateUsed(response, 'donasi_gagal.html')

#function usage test
    def test_use_donateView_func (self) :
        found = resolve('/donate/')
        self.assertEqual(found.func, donate_view)

    def test_use_donateSuccessView_func (self) :
        found = resolve('/donate/success')
        self.assertEqual(found.func, donate_success_view)

    def test_use_donateFailView_func (self) :
        found = resolve('/donate/fail')
        self.assertEqual(found.func, donate_fail_view)

#models test
    def test_models_string_representation(self) :
        newQuery = Donation(program="programA",email_donatur="aaa@bbb.com",nama_donatur="sss",uang_donasi="1000")
        self.assertEqual(str(newQuery),(str(newQuery.program) + " " + str(newQuery.email_donatur) + " " + str(newQuery.uang_donasi)))

    def test_can_submit_query_to_foreign_model(self) :
        register_test = Register.objects.create(nama_donatur="haicuy",donatur_ttl=date(1995,10,9),
                                                donatur_email="ccc@ddd.com",donatur_password="aaaa")
        counting_all_variables = Register.objects.all().count()
        self.assertEqual(counting_all_variables, 1)

    def test_models_can_save_POST_request_not_anon(self) :
        register_test = Register.objects.create(nama_donatur="haicuy",donatur_ttl=date(1995,10,9),
                                                donatur_email="aaa@bbb.com",donatur_password="aaaa")
        response = self.client.post('/donate_post', data = {'program' : 'programA', 'email_donatur' : "aaa@bbb.com",
                                    'nama_donatur' : "sss",'uang_donasi' : "1000", 'id_program' : '1'})

        counting_all_variables = Donation.objects.all().count()
        #nama = Donation.objects.all()[0].nama_donatur
        #self.assertEqual(nama, 'sss')
        self.assertEqual(counting_all_variables, 1)

    def test_models_can_save_POST_request_anon(self) :
        register_test = Register.objects.create(nama_donatur="haicuy",donatur_ttl=date(1995,10,9),
                                                donatur_email="ccc@ddd.com",donatur_password="aaaa")
        response = self.client.post('/donate_post', data = {'program' : "programB",'email_donatur' : "ccc@ddd.com",
                                    'nama_donatur' : "ddd",'uang_donasi' : "2000",'anonymous' : True, 'id_program' : '1'})
        nama = Donation.objects.all()[0].nama_donatur
        self.assertEqual(nama, 'Anonymous')
        counting_all_variables = Donation.objects.all().count()
        self.assertEqual(counting_all_variables, 1)

#cannot save
    def test_models_cannot_save_POST_request(self) :
        register_test = Register.objects.create(nama_donatur="haicuy",donatur_ttl=date(1995,10,9),
                                                donatur_email="c@ddd.com",donatur_password="aaaa")
        response = self.client.post('/donate_post', data = {'program' : "programB",'email_donatur' : "ccc@ddd.com",
                                    'nama_donatur' : "ddd",'uang_donasi' : "2000"})
        counting_all_variables = Donation.objects.all().count()
        self.assertEqual(counting_all_variables, 0)

#check if session exist
    def test_can_donate_with_auth(self) :
        self.c = Client()

        home_response = self.c.get("/")
        self.user = User.objects.create(username="tester",first_name="Abc", last_name="Def", email="tester@mail.com", password="test123", is_active=True, is_staff=True, is_superuser=True)
        self.user.set_password('test123')
        self.user.save()

        self.user = authenticate(username='tester',password='test123')
        self.assertIsNotNone(self.user)

        login = self.c.login(username='tester',password='test123')
        self.assertTrue(login)

        donate_response = self.c.get('/donate/')
        html_response = donate_response.content.decode('utf-8')
        self.assertIn('Abc Def', html_response)
        self.assertIn('tester@mail.com', html_response)