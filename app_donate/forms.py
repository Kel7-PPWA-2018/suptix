from django import forms
from app_donate.models import Donation

class Form_Donasi (forms.Form) :
    class Meta:
        model = Donation
        fields = ['program','nama_donatur','email_donatur','uang_donasi','anonymous',]

    program_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':"ini nama program",
    }

    nama_donatur_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Input your name here',
    }

    email_donatur_attrs = {
        'class' : 'form-control',
        'placeholder': 'fulan123@gmail.com',
        
    }

    donasi_attrs = {
        'type':'number',
        'class' : 'form-control',

    }

    anonymous_attrs = {
        'type' : 'Checkbox',
    }


    program = forms.CharField(label = 'Program Name',initial="ini nama program", widget = forms.TextInput(attrs=program_attrs))
    nama_donatur = forms.CharField(label='Full Name', required=True, widget = forms.TextInput(attrs=nama_donatur_attrs))
    email_donatur = forms.EmailField(label='Email', required=True, widget = forms.EmailInput(attrs=email_donatur_attrs))
    uang_donasi = forms.IntegerField(label='Donation Amount', required=True, widget = forms.NumberInput(attrs=donasi_attrs))
    anonymous = forms.BooleanField(label='Donate Anonymously', required=False, widget = forms.CheckboxInput(attrs=anonymous_attrs))
    id_program = forms.IntegerField(label='Program ID', required=True, widget = forms.HiddenInput())

    def __init__(self, *args, **kwargs):
         super(Form_Donasi, self).__init__(*args, **kwargs)
         self.fields['program'].widget.attrs['readonly'] = True
         self.fields['nama_donatur'].widget.attrs['readonly'] = True
         self.fields['email_donatur'].widget.attrs['readonly'] = True
