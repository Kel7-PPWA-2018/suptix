from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from time import sleep

from .views import index
from .models import News

# Create your tests here.

class ViewsTest(TestCase) :

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_home_html_contain_news(self):
        html_response = self.client.get('/').content.decode('utf8')
        self.assertIn('News', html_response)

    def test_home_html_contain_headlines(self):
        html_response = self.client.get('/').content.decode('utf8')
        self.assertIn('Headlines', html_response)

    def test_home_html_display_models(self):
        title = "Ini adalah sebuah judul untuk test"
        short_desc = "Ini adlaah deskripsi singkat untuk test"
        long_desc = "looooooooooooooooooooooooooooooooooooooooooooong deeeeeeeeeeeeeesvvcccccccccccc"
        date = timezone.now()
        image = 'static/images/nope.png'
        news = News.objects.create(title=title, short_desc=short_desc, long_desc=long_desc, date=date, image=image)

        self.assertEqual(news.title, str(news))
        html_response = self.client.get('/').content.decode('utf8')
        self.assertIn(title, html_response)
        self.assertIn(short_desc, html_response)
        self.assertIn(date.strftime("%B %d, %Y"), html_response)

    def test_news_html_success(self):
        title = "Ini adalah sebuah judul untuk test"
        short_desc = "Ini adlaah deskripsi singkat untuk test"
        long_desc = "looooooooooooooooooooooooooooooooooooooooooooong deeeeeeeeeeeeeesvvcccccccccccc"
        date = timezone.now()
        image = 'static/images/nope.png'
        news = News.objects.create(title=title, short_desc=short_desc, long_desc=long_desc, date=date, image=image)

        #url exist
        response = Client().get('/news/1/')
        self.assertEqual(response.status_code, 200)

        #contain data
        html_response = self.client.get('/news/1/').content.decode('utf8')
        self.assertIn(title, html_response)
        self.assertIn(long_desc, html_response)
        self.assertIn(date.strftime("%B %d, %Y"), html_response)

    def test_news_html_fail(self):
        #url not exist
        response = Client().get('/news/1/')
        self.assertEqual(response.status_code, 302)

    def test_login(self):
        self.c = Client()

        # if not authenticated, there is login button
        new_response = self.c.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Login', html_response)

        # make user object
        self.user = User.objects.create(username='testuser', password='12345', is_active=True, is_staff=True, is_superuser=True) 
        self.user.set_password('hello') 
        self.user.save() 

        # test authenticate user
        self.user = authenticate(username='testuser', password='hello')
        self.assertIsNotNone(self.user)
        
        # manual login
        login = self.c.login(username='testuser', password='hello') 
        self.assertTrue(login)

        # if logged in, there must be Logout and donation list
        new_response = self.c.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Logout', html_response)
        self.assertIn('Daftar Donasi', html_response)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTest, self).setUp()
        chrome_options = Options()

        # gitlab CI settings
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_modal(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        login_button = selenium.find_element_by_id('login')
        login_button.click()

        sleep(2)

        alert = selenium.find_element_by_id('modelId')
        self.assertIn('Login with Google', alert.text)
        alert_button = selenium.find_element_by_id('modalClose')
        alert_button.click()