"""ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import include, path, re_path
from django.contrib import admin
from app_donate.views import donate_view, donate_fail_view, donate_post, donate_success_view

urlpatterns = [
    path('donate_post',donate_post,name='donate_post'),
    path('donate/success',donate_success_view, name = 'donate_success'),
    path('donate/fail',donate_fail_view, name='donate_fail'),
    re_path('^donate/', donate_view, name = 'donate'),
]
