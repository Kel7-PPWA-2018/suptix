from django.urls import re_path
from .views import viewAbout, saveComment
#url for app

urlpatterns = [
    re_path(r'^about/$', viewAbout, name='view'),
    re_path(r'^about/savelul/$', saveComment, name='data')
]