from django.contrib.auth import *
from django.shortcuts import render
from app_donate.forms import Form_Donasi
from .models import Donation
from django.http import HttpResponseRedirect
from app_register.models import Register
import datetime

# Create your views here.
response = {}

def donate_view (request) :
    id_program = ""
    try:
        id_program = request.GET.get('id')
        nama_program = request.GET.get('name')
    except:
        pass

    if (id_program == "") :
        return render(request, 'donasi.html', {'view':False})

    if(request.user.is_authenticated) :
        name = request.user.first_name + " " + request.user.last_name
        return render(request, 'donasi.html', {'view': True, 'form_donasi': Form_Donasi(initial={"program": nama_program, 
        "id_program": id_program, "nama_donatur": name, "email_donatur": request.user.email})})
    else :
        return render(request, 'donasi.html', {'view':False})

def donate_post (request) :
    form = Form_Donasi(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        if ('anonymous' in request.POST) :
            y = True
        else :
            y = False

        if (y == True) :
            response['nama_donatur'] = 'Anonymous'
        else :
            response['nama_donatur'] = request.POST['nama_donatur']

        response['program'] = request.POST['program']
        response['email_donatur'] = request.POST['email_donatur']
        response['uang_donasi'] = request.POST['uang_donasi']
        response['id_program'] = request.POST['id_program']
        response['waktu_donasi'] = datetime.datetime.now()

        query_donasi = Donation(program = response['program'],nama_donatur = response['nama_donatur'],
                                email_donatur = response['email_donatur'],uang_donasi = response['uang_donasi'],
                                waktu_donasi = response['waktu_donasi'], id_program = response['id_program'])
        query_donasi.save()
        return HttpResponseRedirect('/donate/success')
    else :
        return HttpResponseRedirect('/donate/fail')

def donate_success_view (request) :
    return render(request, 'donasi_sukses.html')

def donate_fail_view (request) :
    return render(request, 'donasi_gagal.html')
