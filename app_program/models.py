from django.db import models
from django.utils import timezone

# Create your models here.

class Program(models.Model):
    headline = models.CharField(max_length = 200)
    description = models.TextField(default="Rey Ganteng")
    title = models.CharField(max_length = 50)
    slug_program = models.CharField(max_length = 50, default="rey-ganteng")
    target_uang_terkumpul = models.IntegerField(default=500000)
    image = models.URLField(default='https://files.catbox.moe/c4xl10.jpg', max_length=300)
    time = models.DateTimeField(default = timezone.now)
    target_hari = models.IntegerField(default=30)
    id_news = models.IntegerField(default=0)
