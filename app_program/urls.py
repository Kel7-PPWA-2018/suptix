from django.urls import path, re_path
from .views import program, list_donasi, get_list_donasi

urlpatterns = [
    path(r'program/', program),
    path(r'list_donasi/', list_donasi),
    path(r'get_list_donasi/',get_list_donasi),
]
