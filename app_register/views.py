from django.shortcuts import render
from .models import Register
from .forms import Add_Register_Form
from django.http import HttpResponseRedirect
from django.db import IntegrityError

# Create your views here.
response = {}
def registerView(request):
    html = 'register.html'
    response['register_form'] = Add_Register_Form
    return render(request, html, response)

def saveRegisterView(request):
    form = Add_Register_Form(request.POST or None)
    html = 'registerSave.html'
    try:
        if(request.method == 'POST' and form.is_valid()):
            response['nama'] = request.POST['nama']
            response['ttl'] = request.POST['ttl']
            response['email'] = request.POST['email']
            response['password'] = request.POST['password']
            models = Register(nama_donatur = response['nama'], donatur_ttl = response['ttl'], donatur_email = response['email'], donatur_password = response['password'])
            models.save()
            return render(request, html, response)
        else :
            return HttpResponseRedirect("/register/")
    except IntegrityError:
        return HttpResponseRedirect("/register/")

